import os
import importlib
import json
# Utilities


def install_and_import(package):
    try:
        importlib.import_module(package)
    except ImportError:
        import pip
        if hasattr(pip, 'main'):
            pip.main(['install', package])
        else:
            pip._internal.main(['install', package])
    finally:
        globals()[package] = importlib.import_module(package)


def read_config():
    if not os.path.isfile("config.json"):
        with open("config_example.json", "w") as c:
            c.write(json.dumps({
                "DEV_KEY": "",
                "DEBUG": True,
                "HOST": '0.0.0.0',
                "PORT": 1337,
            }, sort_keys=True, indent=4, separators=(',', ': ')))
        print('Config file config.json not exist!\nPlease edit config_example.json and rename it to config.json')
        exit(1)
    with open("config.json", "r") as f:
        try:
            return json.load(f)
        except Exception as e:
            print(f"Can't read config.json! {e}")
            exit(1)


def __get_value(request, parameter):
    if request.query != {}:
        return request.query.get(parameter, "") if request.query.get(parameter, "") != "" else request.form.get(
            parameter, "")
    else:
        return ""

# Try to import external libs
try:
    from sanic import Sanic
    from sanic import response
except ImportError:
    install_and_import("sanic")
try:
    import requests
except ImportError:
    install_and_import("requests")
try:
    import aiohttp
except ImportError:
    install_and_import("aiohttp")