#!/usr/bin/python3
# Proxy gate by Sociaro
# For support please contact dmitry@sociaro.com
# Pre-installed libs

import time
from json import JSONDecodeError, dumps
from utils import *
# Micro server init
app = Sanic()
# Read config and globals;
config = read_config()

DEBUG = config.get("DEBUG", "true")
HOST = config.get("HOST", "0.0.0.0")
PORT = config.get("PORT", "1337")
DEV_KEY = config.get("DEV_KEY", "123")


@app.listener('before_server_start')
def init(app, loop):
    app.aiohttp_session = aiohttp.ClientSession(loop=loop)


@app.listener('after_server_stop')
def finish(app, loop):
    loop.run_until_complete(app.session.close())
    loop.close()


@app.route("/", methods=['GET', 'POST'])
async def main(request):
    request_start_time = time.time()
    try:
        request_json = request.json if type(request.json) != type(None) else {}
    except JSONDecodeError as e:
        ttime = "%.5fs" % (time.time() - request_start_time)
        return response.text(
            f"Time taken: {ttime}\n"
            f"Response: {e}"
        )
    app_id = request.args.get('app_id', '')
    headers = {
        "authentication": DEV_KEY,
        'Content-Type': 'application/json'
    }
    url = f'https://api2.appsflyer.com/inappevent/{app_id}'
    try:
        async with app.aiohttp_session.post(url=url, data=dumps(request_json), headers=headers) as r:
            ttime = "%.5fs" % (time.time() - request_start_time)
            resp = await r.text()
            return response.text(
                f"Time taken: {ttime}\n"
                f"Request: {request_json}\n"
                f"Response: {resp}"
            )
    except Exception as e:
        ttime = "%.5fs" % (time.time() - request_start_time)
        return response.text(
            f"Time taken: {ttime}\n"
            f"Response: {e}"
        )


@app.route("/ping", methods=['GET', 'POST'])
async def ping(request):
    return response.text(f"Everything looks fine.")


if __name__ == "__main__":
    app.run(host=HOST, debug=DEBUG, port=PORT)
