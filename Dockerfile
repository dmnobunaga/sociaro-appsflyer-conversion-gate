FROM python:3.7
MAINTAINER Dmitry Panchev <dmitry@sociaro.com>
ADD ./main /main
RUN pip3 install sanic requests aiohttp
WORKDIR /main
EXPOSE 1337
ENTRYPOINT ["python"]
CMD ["main.py"]